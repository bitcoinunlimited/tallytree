extern crate tallytree;
use tallytree::generate::generate_tree;
use tallytree::node::NodeRef;
use tallytree::proof::{create_inclusion_exclusion_proof, create_proof_of_vote_count};
use tallytree::serialize::{from_json, to_json};
use tallytree::Validation;

fn main() {
    let tree = generate_tree(
        vec![
            ([0xaa; 32], vec![1, 0]),
            ([0xcc; 32], vec![1, 0]),
            ([0xdd; 32], vec![0, 1]),
        ],
        false,
    )
    .unwrap();
    let v = &Validation::Strict;

    // Serialize and unserialize tree.
    let tree = to_json(&tree).unwrap();
    let tree: NodeRef = from_json(&tree).unwrap();

    let proof = create_inclusion_exclusion_proof(&tree, &[0xaa; 32], v).unwrap();
    let proof_json = to_json(&proof).unwrap();
    println!("Proof that 0xaa's vote was counted: {}", proof_json);

    let proof = create_proof_of_vote_count(&tree, v).unwrap();
    let proof_json = to_json(&proof).unwrap();
    println!(
        "Proof that there are 3 votes in the merkle tally tree: {}",
        proof_json
    );
}
