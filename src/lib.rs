#![crate_name = "tallytree"]
#![warn(missing_docs)]
#![cfg_attr(feature = "fail-on-warnings", deny(warnings))]
#![forbid(unsafe_code)]
#![cfg_attr(feature = "with-benchmarks", feature(test))]

//! # Merkle Tally Tree
//!
//! `tallytree` implements
//! [merkle tally tree](http://bitcoinunlimited.net/blockchain_voting).
//!
//! This is an implementation of Merkle Tally Tree in Rust. Merkle Tally Trees
//! provide an efficient tally of an electronic vote. It scales to millions of
//! votes, while still being able to prove efficiently to every voter that the
//! tally was fair. Using merkle tally tree, you can create:
//!
//!  - Efficient proof that a vote was tallied in a result (vote included).
//!  - Efficient proof that a vote was not tallied in a result (vote excluded).
//!  - Efficient proof of number of ballots included in the tally (no ballot withholding).

#[cfg(feature = "with-benchmarks")]
extern crate test;

/// Functions to generate a merkle tally tree.
pub mod generate;

/// Functions for navigating the merkle tally tree.
pub mod navigate;

/// Node in a merkle tally tree.
pub mod node;

/// Generate and validate proofs using the merkle tally tree.
pub mod proof;

/// Serialize nodes and proofs
pub mod serialize;

/// Functions for tallying votes
pub mod tally;

/// Functions for string conversions.
pub mod utilstring;

/// Functions used for tests and benchmarks.
pub mod utiltest;

/// Functions for hashing nodes in the merkle tally tree.
pub mod hash;

/// A reference to a vote.
pub type VoteReference = [u8; 32];

use crate::node::NodeRef;

/// How to treat Merkle tree (in)consistency
pub enum Validation {
    /// Some inconsistency is allowed, such as leaf nodes casting more than one
    /// vote. This is useful for testing and experimentation.
    Relaxed,
    /// Inconsistency is not allowed and when detected, should be returned as
    /// error.
    Strict,
}
